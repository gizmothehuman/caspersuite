casper.options.trackingBuffer = [];
// Supposedly this can increase test load time but the test fails without it?
// casper.options.pageSettings.loadImages = false;

casper.options.onResourceRequested = function(casper, requestData, request) {

    // white list of tracking services, add here as many as you need
    var trackingServices = [
        'google-analytics.com/collect'
    ];

    trackingServices.forEach(function (needle) {

     // if the request url is in our white list, we push the data to the trackingBuffer array
    if (requestData.url.indexOf(needle) > 0) {
        casper.options.trackingBuffer.push(requestData.url);
        }
    });

};