casper.test.begin('Testing pageview event on the homepage', 1, function (test) {
    casper.start('https://splice.com/'); // open the page
    casper.then(function () {

    	var buffer = casper.options.trackingBuffer,
            params,
            i,
            pageViewTriggered = false;


        for (i = 0; i < buffer.length; i++) {
            params = parseQueryString(buffer[i]);

            if (params.t == 'pageview') {
                pageViewTriggered = true;
            }
        }

        test.assert(pageViewTriggered, 'Pageview event sent');
    });

    casper.run(function () {
        test.done();
    });
});