### WORK IN PROGRESS ###
#### please don't rely on this for verification of any staging or production website changes. ####
#### please DO update this readme as any relevant changes are made.
####

welcome to...
## Automated analytics test verification ##
with casper.js!

### Quickstart ###

1. Clone this repository wherever you like to keep your code, and in your preferred shell/CLI interface, change directories into the new folder.

1. Install the dependencies by running these commands in your shell:

```
npm install phantomjs -g 
npm install casperjs -g
```

1. Run the tests using this command: `casperjs test test.js --includes=casper.options.js,functions.js`

That's it! If you'd like to view the analytics call's payload, do so by uncommenting line 7 of `functions.js`, which looks like this: `// console.log(queries[i])`


### Adding more tests ###

Place additional tests in `test.js`. Casper's API is documented [here](http://docs.casperjs.org/en/latest/modules/casper.html).

This was all shamelessly yoinked from this blog post: https://theiconic.tech/using-phantomjs-and-casperjs-to-monitor-your-google-analytics-implementation-8312a424d1d7